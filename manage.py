from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from agriness.app import create_app

manager = Manager(create_app)
manager.add_command('db', MigrateCommand)

@manager.command
def createdb():    
    from agriness.core.extensions import db 
    db.create_all()

@manager.command
@manager.option(
    "--port",
    dest="port",
    help="Port for run app",
    type=int)
@manager.option(
    "--host",
    dest="host",
    help="Host for run app")
@manager.option(
    "--debug",
    dest="debug",
    help="Debug for run app",
    type=bool)
def runserver(port=None, host=None, debug=None):
    """Method for run project."""
    manager.app.run(**manager.app.config.get_namespace('RUN_'))

if __name__ == '__main__':
    manager.run()
