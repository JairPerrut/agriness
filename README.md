# Agriness CMS API 

## A Simple Content Management System (CMS)

#### Requirements:

- Python >= 3.6;

- Postgres >= 9.6;


#### Installation:

- Clone or download this repo;

- Install the requirements.txt using pip: ``$ pip install -r requirements.txt``

- Configure the connection string of the postgres in the agriness/development_instance/config.py (to development enviroment) and/or agriness/production_instance/config.py (to production enviroment)
 
- Set the variable system FLASK_ENV to "development" or "production", if not set the default will be development.

```
$ export FALSK_ENV=<value>
```

- In project root path execute:
 
``` 
$ python manage.py createdb
```


#### Run:

- ``$ python manage.py runserver``