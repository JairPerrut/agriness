from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_restplus_patched.api import Api
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
cors = CORS()

api = Api(
    title='Agriness',
    version='1.0',
    description='CMS',
)
