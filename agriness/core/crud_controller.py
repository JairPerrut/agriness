from abc import ABC, abstractmethod
from agriness.core.extensions import db

class CRUDController(ABC):
    
    @property
    @abstractmethod
    def model(self):
        pass

    def list(self): 
        return self.model.query.filter()

    def get_by_id(self, pk):
        return self.model.query.get(pk)

    def save(self, model):
        try:
            db.session.add(model)
            db.session.commit()
            return model
        except Exception as err:
            db.session.rollback()
            raise err