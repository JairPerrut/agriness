from agriness.core.extensions import ma, db

class BaseSchema(ma.ModelSchema):    
    class Meta:
        sqla_session = db.session