RUN_DEBUG = True
RUN_USE_RELOADER = True
RUN_HOST = "0.0.0.0"
RUN_PORT = 5000
SQLALCHEMY_ECHO = True
DB_HOST = ""
DB_USER = ""
DB_PASS = ""
DB_NAME = ""
SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s/%s?client_encoding=utf8' % (DB_USER,DB_PASS,DB_HOST,DB_NAME)
SECRET_KEY = "you_secret_key"
