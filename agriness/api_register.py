from agriness.core.extensions import api

from agriness.modules.article.resource import ns as article_ns
from agriness.modules.author.resource import ns as author_ns
from agriness.modules.category.resource import ns as category_ns

api.add_namespace(article_ns, path="/article")
api.add_namespace(author_ns, path="/author")
api.add_namespace(category_ns, path="/category")