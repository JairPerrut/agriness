from os import path, environ

from flask import Flask

from agriness.core.extensions import db, ma, migrate, api, cors

def create_app():    
    instance_path = path.join(
        path.abspath(path.dirname(__file__)), "%s_instance" % environ.get('FLASK_ENV','development')
    )

    app = Flask("agriness_app",
                instance_path=instance_path,
                instance_relative_config=True)

    app.config.from_object('agriness.default_settings')
    app.config.from_pyfile('config.py')
    
    import agriness.api_register
    
    api.init_app(app)
    db.init_app(app)   
    ma.init_app(app)
    cors.init_app(app)
    migrate.init_app(app, db, **app.config.get_namespace("MIGRATE_"))
    return app
