from agriness.core.schema import BaseSchema
from agriness.modules.author.model import Author

class AuthorSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Author

