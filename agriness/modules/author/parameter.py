from flask_restplus_patched.parameters import Parameters

from agriness.modules.author.schema import AuthorSchema


class AddAuthorParameters(Parameters, AuthorSchema):
    class Meta(AuthorSchema.Meta):
        exclude = ('id',)
