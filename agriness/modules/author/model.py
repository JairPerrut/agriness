from agriness.core.extensions import db

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    email = db.Column(db.Unicode)