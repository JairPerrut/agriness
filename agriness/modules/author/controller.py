from agriness.core.crud_controller import CRUDController 
from agriness.modules.author.model import Author

class AuthorController(CRUDController):

    @property
    def model(self):
        return Author
