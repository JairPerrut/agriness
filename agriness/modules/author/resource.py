from flask_restplus._http import HTTPStatus
from flask_restplus_patched import Namespace, Resource

from agriness.modules.author.controller import AuthorController
from agriness.modules.author.parameter import AddAuthorParameters
from agriness.modules.author.schema  import AuthorSchema


ns = Namespace("Author")

@ns.route("/")
class Author(Resource):
    
    @ns.response(AuthorSchema(many=True))
    def get(self):
        return AuthorController().list()

    @ns.response(AuthorSchema(), HTTPStatus.CREATED)
    @ns.response(code=HTTPStatus.UNPROCESSABLE_ENTITY)
    @ns.parameters(AddAuthorParameters(), locations=['json'])   
    def post(self, payload):
        model, errors = AuthorSchema().load(payload)
        if errors:
            ns.abort(HTTPStatus.UNPROCESSABLE_ENTITY)
        return AuthorController().save(model)

@ns.route("/<int:pk>")
@ns.resolve_object('author', lambda kwargs: AuthorController().get_by_id(kwargs.pop('pk')))
@ns.response(code=HTTPStatus.NOT_FOUND, description="Author not found.") 
class AuthorDetail(Resource):

    @ns.response(AuthorSchema())
    @ns.response(code=HTTPStatus.NOT_FOUND)
    def get(self, author):
        if not author:
            ns.abort(HTTPStatus.NOT_FOUND)
        return author
    
    @ns.response(AuthorSchema())
    @ns.response(code=HTTPStatus.UNPROCESSABLE_ENTITY)
    @ns.parameters(AddAuthorParameters(),locations=['json'])
    def put(self,payload, author):
        model, errors = AuthorSchema().load(payload, instance=author)
        if errors:
            ns.abort(HTTPStatus.UNPROCESSABLE_ENTITY)
        return AuthorController().save(model)
