from agriness.core.extensions import db
from agriness.modules.author.model import Author
from agriness.modules.category.model import Category

article_category = db.Table("article_category", db.metadata,
    db.Column("article_id", db.Integer, db.ForeignKey("article.id"), primary_key=True),
    db.Column("category_id",db.Integer, db.ForeignKey("category.id"), primary_key=True),
    db.UniqueConstraint("article_id","category_id")
)

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(255), nullable=False)
    content = db.Column(db.UnicodeText, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey(Author.id), nullable=False)
    author = db.relationship(Author)
    categories = db.relationship(Category, secondary="article_category")
    status = db.Column(db.String, nullable=False, default="draft")
    date_publication = db.Column(db.DateTime)
