from marshmallow.validate import Length
from marshmallow_sqlalchemy.convert import field_for

from agriness.core.extensions import ma
from agriness.core.schema import BaseSchema
from agriness.modules.article.model import Article


class ArticleSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Article
        dump_only = ("status","date_publication","author")
        
    author_id = field_for(Article,"author_id", dump_only=False)


class ArticleDetailsSchema(ArticleSchema):
    class Meta(ArticleSchema.Meta):
        pass
    
    author = ma.Nested("AuthorSchema", exclude=("articles",), dump_only=True)
    categories = ma.Nested("CategorySchema", validates=Length(min=1), many=True)
    
        
class PaginationSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        pass
    
    items = ma.Nested("ArticleDetailsSchema", many=True)
    page = ma.Integer()
    per_page = ma.Integer()
    total_items = ma.Integer()
    total_pages = ma.Integer()