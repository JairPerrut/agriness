from flask_restplus_patched import Namespace, Resource

from agriness.modules.article.controller import ArticleController
from agriness.modules.article.parameter import AddArticleParameters, PaginationParameters
from agriness.modules.article.schema import ArticleSchema, PaginationSchema, ArticleDetailsSchema


ns = Namespace("Article")


@ns.route("/")
class Aticle(Resource):
    
    @ns.response(PaginationSchema())
    @ns.parameters(PaginationParameters(),locations=['query'])
    def get(self, query):
        controller = ArticleController()
        page = query.pop('page',1)
        per_page = query.pop('per_page',20)
        reverse = query.pop('reverse', False)
        if query:
            stmt = controller.filter(query)
        else:
            stmt = controller.list()
        if reverse:
            stmt = stmt.order_by(controller.model.date_publication.desc())
        else:
            stmt = stmt.order_by(controller.model.date_publication)
        models = stmt.paginate(page, per_page, False)
        return dict(per_page=models.per_page, total_items=models.total, page=models.page, total_pages=models.pages, items=ArticleDetailsSchema().dump(models.items, many=True).data), 200

    @ns.response(ArticleDetailsSchema(), 201)
    @ns.parameters(AddArticleParameters(),locations=['json'])
    def post(self, payload):
        model, errors = ArticleSchema().load(payload)
        if errors:
            return errors, 422
        return ArticleController().save(model)


@ns.route("/<int:pk>")
@ns.resolve_object('article', lambda kwargs: ArticleController().get_by_id(kwargs.pop('pk')))
class ArticleDetail(Resource):
    
    @ns.response(ArticleDetailsSchema())
    def get(self, article):
        if not article:
            ns.abort(404)
        return article

    @ns.response(ArticleDetailsSchema())
    @ns.parameters(AddArticleParameters(),locations=['json'])
    def put(self, payload, article):
        model, errors = ArticleSchema().load(payload, instance=article)
        if errors:
            ns.abort(422, errors)
        return ArticleController().save(model)
    

@ns.route("/<int:pk>/publish")
@ns.resolve_object('article', lambda kwargs: ArticleController().get_by_id(kwargs.pop('pk')))
class ArticlePublish(Resource):
    
    @ns.response(ArticleDetailsSchema())
    def put(self, article):
        if not article:
            ns.abort(404)
        return ArticleController().publish(article)
