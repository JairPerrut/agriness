from flask_restplus_patched.parameters import Parameters
from marshmallow import validate, fields

from agriness.core.extensions import ma
from agriness.modules.article.schema import ArticleSchema


class AddArticleParameters(Parameters, ArticleSchema):
    class Meta(ArticleSchema.Meta):
        exclude=('id',)
    
    categories = fields.List(fields.Integer())

class PaginationParameters(Parameters):
    
    per_page = ma.Integer(
        description="limit a number of items (allowed range is 1-100), default is 20.",
        validate=validate.Range(min=1, max=100)
    )
    page = ma.Integer(
        description="a number of items to skip, default is 1.",
        validate=validate.Range(min=1)
    )
    search = ma.String(
        description="search a word on the title and body in the articles."
    )
    author = ma.String(
        description="a name of author to search."
    )
    category = ma.String(
        description="a category to search."
    )
    reverse = ma.Boolean(
        description="to list articles in reverse chronological order, default is false."
    )