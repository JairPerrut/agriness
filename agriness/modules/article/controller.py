from datetime import datetime

from agriness.core.crud_controller import CRUDController 
from agriness.modules.article.model import Article
from agriness.modules.author.model import Author
from agriness.modules.category.model import Category
from sqlalchemy.sql.functions import func


class ArticleController(CRUDController):
    
    @property
    def model(self):
        return Article
    
    def filter(self, filters):
        stmt = self.model.query
        for key, value in filters.items():
            if key == "author":
                stmt = stmt.join(Article.author).filter(Author.name.ilike(value + "%"))
            if key == "category":
                stmt = stmt.join(Article.categories).filter(Category.name.ilike(value+ "%"))
            if key == "search":
                stmt = stmt.filter(func.to_tsvector(func.concat_ws(' ', Article.content, Article.title)).match(value, postgresql_regconfig='portuguese'))
        return stmt
        
#===============================================================================
#     def get_by_author(self, name):
#         return self.model.query.join(Article.author).filter(Author.name.ilike(name + "%"))
# 
#     def get_by_category(self, name):
#         return self.model.query.join(Article.categories).filter(Category.name.ilike(name + "%"))
#     
#     def full_text_search(self, text):
#         return self.model.query.filter(
#             func.to_tsvector(func.concat_ws(' ', Article.content, Article.title)).match(text, postgresql_regconfig='portuguese')
#             )
#===============================================================================

    def publish(self, article):
        if article.status == "published":
            raise Exception("Already published")
        article.status = "published"
        article.date_publication = datetime.now()
        self.save(article)
        return article
