from flask_restplus_patched.parameters import Parameters

from agriness.modules.category.schema import CategorySchema


class AddCategoryParameters(Parameters, CategorySchema):
    class Meta(CategorySchema.Meta):
        exclude=('id',)

