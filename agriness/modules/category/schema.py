from agriness.core.schema import BaseSchema
from agriness.modules.category.model import Category


class CategorySchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Category
