from agriness.core.crud_controller import CRUDController 
from agriness.modules.category.model import Category

class CategoryController(CRUDController):
    
    @property
    def model(self):
        return Category