from flask_restplus_patched import Namespace, Resource

from agriness.modules.category.controller import CategoryController
from agriness.modules.category.parameter import AddCategoryParameters
from agriness.modules.category.schema import CategorySchema


ns = Namespace("category")

@ns.route("/")
class Category(Resource):
    
    @ns.response(CategorySchema(many=True))
    def get(self):
        return CategoryController().list()
    
    @ns.response(CategorySchema(), 201)
    @ns.parameters(AddCategoryParameters(), locations=['json'])
    def post(self, payload):
        model, errors = CategorySchema().load(payload)
        if errors:
            ns.abort(422, errors)
        return CategoryController().save(model)


@ns.route("/<int:pk>")
@ns.resolve_object('category', lambda kwargs: CategoryController().get_by_id(kwargs.pop('pk')))
class CategoryDetail(Resource):

    @ns.response(CategorySchema())
    def get(self, category):
        return category
    
    @ns.response(CategorySchema())
    @ns.parameters(AddCategoryParameters(), locations=['json'])
    def put(self, payload, category):
        model, errors = CategorySchema().load(payload, instance=category)
        if errors:
            return errors, 422
        return CategoryController().save(model)
