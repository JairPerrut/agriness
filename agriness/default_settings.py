import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

SWAGGER_UI_DOC_EXPANSION = "list"
SQLALCHEMY_IGNORE_TABLES = [
    "alembic",
    ]

MIGRATE_INCLUDE_OBJECT = lambda object, name, type_, reflected, compare_to : reflected or name not in SQLALCHEMY_IGNORE_TABLES
